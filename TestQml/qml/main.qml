import QtQuick 2.0
import magic.lib 1.0

Rectangle
{
    width: 360
    height: 360

//    MagicModel
//    {
//        id: myList
//    }

    GenreModel
    {
        id: genreModel
    }

    GridView
    {
        id: gridView
        anchors.fill: parent
        delegate: gridDelegate
        model: genreModel

        cellHeight: 150
        cellWidth: 150
    }

    Component
    {
        id: gridDelegate
        Rectangle
        {
            color: roleColor
            radius: 10
            border.width: 3
            height: gridView.cellHeight
            width: gridView.cellWidth

            Image {
                id: thisIsAImage
                anchors.fill: parent
                anchors.margins: 10
//                source: roleImageFile === "..." ? "" : roleImageFile

            }

            Rectangle
            {
                anchors.fill: thisIsAText
                color: "#80ffffff"
                radius: 3

            }

            Text {
                id: thisIsAText
                anchors.bottom: parent.bottom
                anchors.margins: 5
                anchors.left: parent.left
                anchors.right: parent.right
                text: roleTitle
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
                font.pixelSize: 20
            }
        }
    }
}

import QtQuick 2.0

Rectangle
{
    property alias text: label.text
    signal clicked

    id: button

    width: 50
    height: 50

    color: "purple"


    Text {
        id: label
        anchors.fill: parent
        anchors.centerIn: parent
        color: "white"
        font.pixelSize: 15
    }

    MouseArea
    {
        anchors.fill: parent

        onPressed:
        {
            increaseAnimation.restart()
        }

        onReleased:
        {
            decreaseAnimation.restart()
        }

        onClicked: button.clicked()
    }


    ParallelAnimation
    {
        id: increaseAnimation

        NumberAnimation {
            target: button
            property: "scale"

            duration: 100
            from: 1.0
            to: 1.3
            paused: false
        }
    }

    ParallelAnimation
    {
        id: decreaseAnimation

        NumberAnimation {
            target: button
            property: "scale"

            duration: 100
            from: 1.3
            to: 1.0
            paused: false
        }
    }

}

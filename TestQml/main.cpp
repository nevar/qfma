#include <QtGui/QGuiApplication>
#include <QtQuick>
#include "qtquick2applicationviewer.h"
#include "FMA/Model/CuratorsModel.h"
#include "FMA/Model/FGenreModel.h"
#include "FMA/FSession.h"
#include "FMA/FRequest/FRequestCurators.h"
#include "FMA/Model/FilterModel.h"

int main(int argc, char *argv[])
{
    FSession::instance().setApiKey("NKC2LLXO1TXCQMTZ");

    QGuiApplication app( argc, argv );
    qmlRegisterType<FilterModel>("magic.lib", 1, 0, "FilteraModel");
    qmlRegisterType<FGenreModel>("magic.lib", 1, 0, "GenreModel");
    qmlRegisterType<CuratorsModel>("magic.lib", 1, 0, "CuratorsModel");

    QtQuick2ApplicationViewer viewer;
    viewer.setMainQmlFile(QStringLiteral("qml/main.qml"));
    viewer.showExpanded();

    return app.exec();
}

#include "CuratorsModel.h"
#include "FMA/DataProvider/FCuratorDataProvider.h"

CuratorsModel::CuratorsModel(QObject *parent)
    : QAbstractListModel(parent)
    , m_dataProvider(this)
{
    m_isSortedAscending = true;
    connect( &m_dataProvider, SIGNAL(curatorChanged(int)), this, SLOT(onCuratorChanged(int)) );
    connect( &m_dataProvider, SIGNAL(beginInsertCurator(int,int)), this, SLOT(onBeginInsertCurator(int,int)) );
    connect( &m_dataProvider, SIGNAL(endInsertCurator()), this, SLOT(onEndInsertCurator()) );
    connect( &m_dataProvider, SIGNAL(beginReset()), this, SLOT(onBeginReset()) );
    connect( &m_dataProvider, SIGNAL(endReset()), this, SLOT(onEndReset()) );
    connect( this, SIGNAL(sortingFieldChanged(Roles)), &m_dataProvider, SLOT(onSortingFieldChanged()) );
}

int CuratorsModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)

    return m_dataProvider.maxCuratorsCount();
}

QVariant CuratorsModel::data(const QModelIndex &index, int role) const
{
    if ( !index.isValid() || index.row() >= m_dataProvider.maxCuratorsCount() )
    {
        return QVariant();
    }

    FCurator curator( m_dataProvider.getCurator( index.row() ) );

#define CURATOR_CASE_STRING( VALUE, FUCTION ) case VALUE: return FUCTION.isNull() ? "..." : FUCTION; break
#define CURATOR_CASE_INT( VALUE, FUCTION ) case VALUE: return FUCTION < 0 ? QVariant("") : QVariant(FUCTION); break

    switch (role) {
    CURATOR_CASE_STRING( ROLE_BIO, curator.bio() );
    CURATOR_CASE_INT( ROLE_COMMENTS, curator.comments() );
    CURATOR_CASE_STRING( ROLE_DATE_CREATED, curator.dateCreated() );
    CURATOR_CASE_INT( ROLE_FAVORITES, curator.favorites() );
    CURATOR_CASE_STRING( ROLE_HANDLE, curator.handle() );
    CURATOR_CASE_INT( ROLE_ID, curator.id() );
    CURATOR_CASE_STRING( ROLE_IMAGE_FILE, curator.imageFile() );
    CURATOR_CASE_INT( ROLE_PLAYLISTS, curator.playlists() );
    CURATOR_CASE_STRING( ROLE_SILE_URL, curator.siteUrl() );
    CURATOR_CASE_STRING( ROLE_TAG_LINE, curator.tagline() );
    CURATOR_CASE_STRING( ROLE_TITLE, curator.title() );
    CURATOR_CASE_STRING( ROLE_TYPE, curator.type() );
    CURATOR_CASE_STRING( ROLE_URL, curator.url() );

    default:
        break;
    }

    return QVariant();
}

QHash<int, QByteArray> CuratorsModel::roleNames() const
{
    QHash<int, QByteArray> hash;

    hash.insert( ROLE_ID, QString("roleId").toLatin1() );
    hash.insert( ROLE_HANDLE, QString("roleHandle").toLatin1() );
    hash.insert( ROLE_SILE_URL, QString("roleSiteUrl").toLatin1() );
    hash.insert( ROLE_URL, QString("roleUrl").toLatin1() );
    hash.insert( ROLE_IMAGE_FILE, QString("roleImageFile").toLatin1() );
    hash.insert( ROLE_TYPE, QString("roleType").toLatin1() );
    hash.insert( ROLE_TITLE, QString("roleTitle").toLatin1() );
    hash.insert( ROLE_TAG_LINE, QString("roleTagLine").toLatin1() );
    hash.insert( ROLE_BIO, QString("roleBio").toLatin1() );
    hash.insert( ROLE_FAVORITES, QString("roleFavorites").toLatin1() );
    hash.insert( ROLE_COMMENTS, QString("roleComments").toLatin1() );
    hash.insert( ROLE_PLAYLISTS, QString("rolePlaylists").toLatin1() );
    hash.insert( ROLE_DATE_CREATED, QString("roleDateCreated").toLatin1() );

    return hash;
}

bool CuratorsModel::isSortedAscending() const
{
    return m_isSortedAscending;
}

void CuratorsModel::setisSortedAscending(const bool asc)
{
    if ( m_isSortedAscending != asc )
    {
        m_isSortedAscending = asc;
        emit isSortedAscendingChanged(m_isSortedAscending);
    }
}

CuratorsModel::Roles CuratorsModel::sortingField() const
{
    return m_sortingField;
}

void CuratorsModel::setSortingField(CuratorsModel::Roles field)
{
    if( m_sortingField != field )
    {
        m_sortingField = field;
        emit sortingFieldChanged(m_sortingField);
    }
}

void CuratorsModel::onCuratorChanged(int curatorIndex)
{
    emit dataChanged( index(curatorIndex), index(curatorIndex) );
}

void CuratorsModel::onBeginInsertCurator( int curatorStartIndex, int curatorEndIndex )
{
    beginInsertRows( QModelIndex(), curatorStartIndex, curatorEndIndex );
}

void CuratorsModel::onEndInsertCurator()
{
    endInsertRows();
}

void CuratorsModel::onBeginReset()
{
    beginResetModel();
}

void CuratorsModel::onEndReset()
{
    endResetModel();
}

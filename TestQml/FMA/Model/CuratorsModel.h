#ifndef MAGICMODEL_H
#define MAGICMODEL_H

#include "FMA/DataProvider/FCuratorDataProvider.h"
#include <QAbstractListModel>
#include <QObject>

class CuratorsModel : public QAbstractListModel
{
    Q_OBJECT
    Q_ENUMS(Roles)

    Q_PROPERTY(bool isSortedAscending READ isSortedAscending WRITE setisSortedAscending NOTIFY isSortedAscendingChanged)
    Q_PROPERTY(Roles sortingField READ sortingField WRITE setSortingField NOTIFY sortingFieldChanged)
    // TODO clear requesta sent before changing sorting

public:
    enum Roles
    {
        ROLE_ID = 0,
        ROLE_HANDLE,
        ROLE_SILE_URL,
        ROLE_URL,
        ROLE_IMAGE_FILE,
        ROLE_TYPE,
        ROLE_TITLE,
        ROLE_TAG_LINE,
        ROLE_BIO,
        ROLE_FAVORITES,
        ROLE_COMMENTS,
        ROLE_PLAYLISTS,
        ROLE_DATE_CREATED
    };

public:
    explicit CuratorsModel(QObject *parent = 0);

    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    virtual QHash<int,QByteArray> roleNames() const;

    bool isSortedAscending() const;
    void setisSortedAscending(const bool asc);

    Roles sortingField() const;
    void setSortingField(Roles field);

signals:
    void isSortedAscendingChanged(const bool asc);
    void sortingFieldChanged(Roles sortingField);
    
public slots:
    void onCuratorChanged( int curatorIndex );
    void onBeginInsertCurator( int curatorStartIndex, int curatorEndIndex );
    void onEndInsertCurator();
    void onBeginReset();
    void onEndReset();

private:
    FCuratorDataProvider m_dataProvider;
    bool m_isSortedAscending;
    Roles m_sortingField;
};

#endif // MAGICMODEL_H

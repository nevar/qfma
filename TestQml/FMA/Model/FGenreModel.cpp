#include "FGenreModel.h"
#include "FMA/FGenre.h"

FGenreModel::FGenreModel(QObject *parent) :
    QAbstractListModel(parent)
{
    connect( &m_dataProvider, SIGNAL(genreChanged(int)), this, SLOT(onGenreChanged(int)) );
    connect( &m_dataProvider, SIGNAL(beginInsertGenre(int,int)), this, SLOT(onBeginInsertGenre(int,int)) );
    connect( &m_dataProvider, SIGNAL(endInsertGenre()), this, SLOT(onEndInsertGenre()) );
    connect( &m_dataProvider, SIGNAL(beginReset()), this, SLOT(onBeginReset()) );
    connect( &m_dataProvider, SIGNAL(endReset()), this, SLOT(onEndReset()) );
}

int FGenreModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)

    return m_dataProvider.maxGenresCount();
}

QVariant FGenreModel::data(const QModelIndex &index, int role) const
{
    if ( !index.isValid() || index.row() >= m_dataProvider.maxGenresCount() )
    {
        return QVariant();
    }

    FGenre genre( m_dataProvider.getGenre( index.row() ) );

#define CURATOR_CASE_STRING( VALUE, FUCTION ) case VALUE: return FUCTION.isNull() ? "..." : FUCTION; break
#define CURATOR_CASE_INT( VALUE, FUCTION ) case VALUE: return FUCTION < 0 ? QVariant("") : QVariant(FUCTION); break

    switch (role) {
    CURATOR_CASE_INT( ROLE_ID, genre.id() );
    CURATOR_CASE_INT( ROLE_PARENT_ID, genre.parentId() );
    CURATOR_CASE_STRING( ROLE_TITLE, genre.title() );
    CURATOR_CASE_STRING( ROLE_HANDLE, genre.handle() );
    CURATOR_CASE_STRING( ROLE_COLOR, genre.color() );

    default:
        break;
    }

    return QVariant();
}

QHash<int, QByteArray> FGenreModel::roleNames() const
{
    QHash<int, QByteArray> hash;

    hash.insert( ROLE_ID, QString("roleId").toLatin1() );
    hash.insert( ROLE_PARENT_ID, QString("roleParentId").toLatin1() );
    hash.insert( ROLE_TITLE, QString("roleTitle").toLatin1() );
    hash.insert( ROLE_HANDLE, QString("roleHandle").toLatin1() );
    hash.insert( ROLE_COLOR, QString("roleColor").toLatin1() );

    return hash;
}

FilterModel* FGenreModel::getFilterModel( const int parentId )
{
    if ( !m_modelFilters.contains( parentId ) )
    {
        FilterModel* pModel = new FilterModel(this);
        pModel->setRoleFilter( ROLE_PARENT_ID, parentId );
        m_modelFilters.insert( parentId, pModel );
    }

    return m_modelFilters[parentId];
}

void FGenreModel::onGenreChanged(int genreIndex)
{
    emit dataChanged( index(genreIndex), index(genreIndex) );
}

void FGenreModel::onBeginInsertGenre( int genreStartIndex, int genreEndIndex )
{
    beginInsertRows( QModelIndex(), genreStartIndex, genreEndIndex );
}

void FGenreModel::onEndInsertGenre()
{
    endInsertRows();
}

void FGenreModel::onBeginReset()
{
    beginResetModel();
}

void FGenreModel::onEndReset()
{
    endResetModel();
}

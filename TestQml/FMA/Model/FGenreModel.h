#ifndef FGENREMODEL_H
#define FGENREMODEL_H

#include "FMA/DataProvider/FGenreDataProvider.h"
#include "FMA/Model/FilterModel.h"
#include <QAbstractListModel>

class FGenreModel : public QAbstractListModel
{
    Q_OBJECT

    enum Roles
    {
        ROLE_ID = 0,
        ROLE_PARENT_ID,
        ROLE_TITLE,
        ROLE_HANDLE,
        ROLE_COLOR
    };

public:    
    explicit FGenreModel(QObject *parent = 0);

    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    virtual QHash<int,QByteArray> roleNames() const;

    Q_INVOKABLE FilterModel* getFilterModel( const int parentId );

signals:
    
public slots:
    void onGenreChanged(int genreIndex );
    void onBeginInsertGenre(int genreStartIndex, int genreEndIndex );
    void onEndInsertGenre();
    void onBeginReset();
    void onEndReset();

private:
    FGenreDataProvider m_dataProvider;
    QHash<int,FilterModel*> m_modelFilters; // parent id to filter model pointer
};

#endif // FGenreModel_H

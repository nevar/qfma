#ifndef FILTERMODEL_H
#define FILTERMODEL_H

#include <QObject>
#include <QSortFilterProxyModel>

class FilterModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    explicit FilterModel( QObject* parent = NULL );

    void setRoleFilter( int role, int value );

    virtual bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;

private:
    int m_filterValue;
};

#endif // FILTERMODEL_H

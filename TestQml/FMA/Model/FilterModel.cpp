#include "FilterModel.h"

FilterModel::FilterModel(QObject *parent)
    : QSortFilterProxyModel(parent)
{
    QAbstractItemModel* pModel = dynamic_cast<QAbstractItemModel*>(parent);
    if ( pModel )
        setSourceModel( pModel );
}

void FilterModel::setRoleFilter(int role, int value)
{
    setFilterRole(role);
    m_filterValue = value;
}

bool FilterModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    if ( sourceModel()->data( sourceModel()->index( source_row, 0 ), filterRole() ) == m_filterValue )
    {
        return true;
    }

    return false;
}

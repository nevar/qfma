#ifndef FGENRE_H
#define FGENRE_H

#include <QString>
#include <QJsonObject>

class FGenre
{
public:
    FGenre();
    FGenre(const QJsonObject& jObject);

    int id() const;
    int parentId() const;
    QString title() const;
    QString handle() const;
    QString color() const;

private:
    int m_genreId;
    int m_genreParentId;
    QString m_genreTitle;
    QString m_genreHandle;
    QString m_genreColor;


};

#endif // FGENRE_H

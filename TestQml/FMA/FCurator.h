#ifndef FCURATOR_H
#define FCURATOR_H
#include <QObject>
#include <QJsonObject>

class FCurator
{
public:
    FCurator();
    FCurator(const QJsonObject& jObject);

    int id() const;
    QString handle() const;
    QString url() const;
    QString siteUrl() const;
    QString imageFile() const;
    QString type() const;
    QString title() const;
    QString tagline() const;
    QString bio() const;
    int favorites() const;
    int comments() const;
    int playlists() const;
    QString dateCreated() const;

private:
    QString m_handle;
    QString m_url;
    QString m_siteUrl;
    QString m_imageFile;
    QString m_type;
    QString m_title;
    QString m_tagline;
    QString m_bio;
    QString m_dateCreated;
    int m_id;
    int m_favorites;
    int m_comments;
    int m_playlists;
};

#endif // FCURATOR_H

#include "FGenre.h"

FGenre::FGenre()
{
    m_genreId = -1;
    m_genreParentId = -1;
}

FGenre::FGenre(const QJsonObject &jObject)
{
    m_genreId = jObject.value("genre_id").toString().toInt();
    m_genreParentId = jObject.value("genre_parent_id").toString().toInt();
    m_genreTitle = jObject.value("genre_title").toString();
    m_genreHandle = jObject.value("genre_handle").toString();
    m_genreColor = jObject.value("genre_color").toString();

    if ( m_genreTitle.isNull() ) m_genreTitle = "";
    if ( m_genreHandle.isNull() ) m_genreHandle = "";
    if ( m_genreColor.isNull() ) m_genreColor = "";
}

int FGenre::id() const
{
    return m_genreId;
}

int FGenre::parentId() const
{
    return m_genreParentId;
}

QString FGenre::title() const
{
    return m_genreTitle;
}

QString FGenre::handle() const
{
    return m_genreHandle;
}

QString FGenre::color() const
{
    return m_genreColor;
}

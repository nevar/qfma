#include "FSession.h"
#include <QNetworkRequest>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

FSession::FSession(QObject *parent)
    : QObject(parent)
    , m_networkAccessManager(this)
{
    connect( &m_networkAccessManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(onFinished(QNetworkReply*)) );
}

FSession &FSession::instance()
{
    static FSession session;
    return session;
}

QString FSession::apiKey() const
{
    return m_apiKey;
}

void FSession::setApiKey(const QString &key)
{
    if ( m_apiKey != key )
    {
        m_apiKey = key;
        emit apiKeyChanged( m_apiKey );
    }
}

void FSession::sendFRequest( const FRequest & fRequest )
{
    // Checking if request was already sent before acquiring mutex gives better performance in dropping duplicated requests
    if ( m_sentRequests.contains(fRequest) )
    {
        return;
    }

    QMutexLocker mapLocker( &m_mapLock );
    if ( m_sentRequests.contains(fRequest) )
    {
        return;
    }

    QNetworkRequest networkRequest;
    QString strUrl("http://freemusicarchive.org/api/get/");

    strUrl.append( fRequest.getStringRequestType() + "." + fRequest.getStringResponseType() );
    strUrl.append("?api_key=" + m_apiKey);

    if ( fRequest.page() >= 0 )
    {
        strUrl.append("&page=" + QString::number( fRequest.page() ) );
    }
    else
    {
        strUrl.append("&page=0");
    }

    strUrl.append("&limit=" + QString::number( fRequest.limit() ) );

    if ( !fRequest.sortId().isEmpty() )
    {
        strUrl.append("&sort_by=" + fRequest.sortId() );
        QString sortType( "desc" );
        if ( fRequest.isSortedAscending() )
        {
            sortType = "asc";
        }
        strUrl.append("&sort_dir=" + sortType );
    }

    networkRequest.setUrl( QUrl(strUrl) );
    QNetworkReply * pReply = m_networkAccessManager.get( networkRequest );
    qDebug() << __LINE__ << "sending request, index: " << fRequest.requestedIndex() << " page: " << fRequest.page();

    m_sentRequests.insert( fRequest, 1 );
    m_requestsMap.insert( pReply, fRequest );
}

void FSession::onFinished(QNetworkReply * pReply)
{
    FRequest fRequest = m_requestsMap.value(pReply);
    qDebug() << __LINE__ << "request finished, index: " << fRequest.requestedIndex() << " page: " << fRequest.page();

    int index = fRequest.limit() * ( fRequest.page() - 1 );

    QByteArray byteReply = pReply->readAll();
    QJsonDocument jDoc = QJsonDocument::fromJson( byteReply );
    QJsonObject jObject = jDoc.object();
    QJsonArray jArray = jObject.value("dataset").toArray();
    int totalPages = jObject.value("total_pages").toDouble();

    int max = jObject.value("total").toString().toInt();

    QString replyType( jObject.value( "title" ).toString() );
    if ( replyType == "Free Music Archive - Genres" )
    {
        for( QJsonArray::iterator i = jArray.begin(); i != jArray.end(); ++i )
        {
            FGenre genre( (*i).toObject() );
            emit genreLoaded( genre, index++, max );
        }
    }
    else
    {
        for( QJsonArray::iterator i = jArray.begin(); i != jArray.end(); ++i )
        {
            FCurator curator( (*i).toObject() );
            emit curatorLoaded( curator, index++, max );
        }
    }

    QMutexLocker mapLocker( &m_mapLock );
    m_requestsMap.remove(pReply);
    m_sentRequests.remove(fRequest);
    mapLocker.unlock();

    if ( fRequest.requestedIndex() == -1 )
    {
        for ( int i = fRequest.page() + 1; i <= totalPages; ++i )
        {
            FRequest f( fRequest );
            f.setRequestIndex( (i - 1) * f.limit() );
            sendFRequest(f);
        }
    }

    pReply->deleteLater();
}

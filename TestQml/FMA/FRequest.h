#ifndef FREQUEST_H
#define FREQUEST_H

#include <QString>
#include <QHash>

class FRequest
{
public:
    enum RequestType
    {
        REQUEST_TYPE_CURATORS,
        REQUEST_TYPE_GENRES
    };

    enum ResponseType
    {
        RESPONSE_TYPE_XML,
        RESPONSE_TYPE_JSON
    };

private:
    FRequest& internalCopy(const FRequest & other);

public:
    FRequest(int requestedIndex = 0);
    FRequest(const FRequest & other);
    FRequest& operator=( const FRequest & other );
    bool operator==( const FRequest & other ) const;
    bool operator!=( const FRequest & other ) const;

    int limit() const;
    void setLimit( const int newLimit );

    ResponseType responseType() const;
    void setResponseType( const ResponseType responseType );
    QString getStringResponseType() const;

    RequestType requestType() const;
    void setRequestType( const RequestType requestType );
    QString getStringRequestType() const;

    int requestedIndex() const;

    void setRequestIndex( const int newIndex );

    int page() const;

    void setSordId( const QString & newSortId );

    QString sortId() const;

    void setSortedAscending( bool state );

    bool isSortedAscending() const;

private:
    RequestType m_requestType;
    ResponseType m_responseType;
    int m_limit;
    int m_page;
    int m_requestedIndex;

protected:
    QString m_sortId;
    bool m_isSortedAscending;

public:
    static QHash<RequestType,QString> initRequestTypes();
    static QHash<ResponseType,QString> initResponseTypes();

private:
    static QHash<RequestType,QString> m_requestTypes;
    static QHash<ResponseType,QString> m_responseTypes;
};

uint qHash(const FRequest & fRequest);

#endif // FMAREQUEST_H

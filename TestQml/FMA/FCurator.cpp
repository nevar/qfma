#include "FCurator.h"

FCurator::FCurator()
{
    m_id = -1;
    m_favorites = -1;
    m_comments = -1;
    m_playlists = -1;
}

FCurator::FCurator(const QJsonObject &jObject)
{
    m_id = jObject.value("curator_id").toString().toInt();
    m_favorites = jObject.value("curator_favorites").toString().toInt();
    m_comments = jObject.value("curator_comments").toString().toInt();
    m_playlists = jObject.value("curator_playlists").toString().toInt();

    m_handle = jObject.value("curator_handle").toString();
    m_url = jObject.value("curator_url").toString();
    m_siteUrl = jObject.value("curator_site_url").toString();
    m_type = jObject.value("curator_type").toString();
    m_title = jObject.value("curator_title").toString();
    m_tagline = jObject.value("curator_tagline").toString();
    m_bio = jObject.value("curator_bio").toString();
    m_dateCreated = jObject.value("curator_date_created").toString();
    m_imageFile = jObject.value("curator_image_file").toString();

    if ( m_imageFile.isNull() ) m_imageFile = "";
    if ( m_handle.isNull() ) m_handle = "";
    if ( m_url.isNull() ) m_url = "";
    if ( m_siteUrl.isNull() ) m_siteUrl = "";
    if ( m_type.isNull() ) m_type = "";
    if ( m_title.isNull() ) m_title = "";
    if ( m_tagline.isNull() ) m_tagline = "";
    if ( m_bio.isNull() ) m_bio = "";
    if ( m_dateCreated.isNull() ) m_dateCreated = "";
}

int FCurator::id() const
{
    return m_id;
}

QString FCurator::handle() const
{
    return m_handle;
}

QString FCurator::url() const
{
    return m_url;
}

QString FCurator::siteUrl() const
{
    return m_siteUrl;
}

QString FCurator::imageFile() const
{
    return m_imageFile;
}

QString FCurator::type() const
{
    return m_type;
}

QString FCurator::title() const
{
    return m_title;
}

QString FCurator::tagline() const
{
    return m_tagline;
}

QString FCurator::bio() const
{
    return m_bio;
}

int FCurator::favorites() const
{
    return m_favorites;
}

int FCurator::comments() const
{
    return m_comments;
}

int FCurator::playlists() const
{
    return m_playlists;
}

QString FCurator::dateCreated() const
{
    return m_dateCreated;
}

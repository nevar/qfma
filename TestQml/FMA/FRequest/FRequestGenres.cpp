#include "FRequestGenres.h"

FRequestGenres::FRequestGenres(int curatorIndex)
    : FRequest(curatorIndex)
{
    setRequestType( REQUEST_TYPE_GENRES );
    m_sortId = "genre_color";
    m_isSortedAscending = true;
}

#ifndef FREQUESTCURATORS_H
#define FREQUESTCURATORS_H

#include "FMA/FRequest.h"
#include "FMA/Model/CuratorsModel.h"
#include <QHash>

class FRequestCurators : public FRequest
{
public:
    FRequestCurators( int curatorIndex = 0, CuratorsModel::Roles role = CuratorsModel::ROLE_TITLE );

    void fillRoleToStringMap();
    static QHash<CuratorsModel::Roles,QString> ms_roleToString;
};

#endif // FREQUESTCURATORS_H

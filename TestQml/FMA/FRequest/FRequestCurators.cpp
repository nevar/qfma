#include "FRequestCurators.h"

QHash<CuratorsModel::Roles,QString> FRequestCurators::ms_roleToString;

FRequestCurators::FRequestCurators(int curatorIndex, CuratorsModel::Roles role)
    : FRequest(curatorIndex)
{
    if ( ms_roleToString.contains(role) )
    {
        setSordId( ms_roleToString[role] );
    }
    else
    {
        setSordId("");
    }

    fillRoleToStringMap();
    setRequestType( REQUEST_TYPE_CURATORS );
    m_sortId = "curator_title";
    m_isSortedAscending = true;
}

void FRequestCurators::fillRoleToStringMap()
{
    if( ms_roleToString.isEmpty() )
    {
        ms_roleToString.insert(CuratorsModel::ROLE_ID,"curator_id");
        ms_roleToString.insert(CuratorsModel::ROLE_FAVORITES,"curator_favorites");
        ms_roleToString.insert(CuratorsModel::ROLE_COMMENTS,"curator_comments");
        ms_roleToString.insert(CuratorsModel::ROLE_PLAYLISTS,"curator_playlists");
        ms_roleToString.insert(CuratorsModel::ROLE_HANDLE,"curator_handle");
        ms_roleToString.insert(CuratorsModel::ROLE_URL,"curator_url");
        ms_roleToString.insert(CuratorsModel::ROLE_SILE_URL,"curator_site_url");
        ms_roleToString.insert(CuratorsModel::ROLE_TYPE,"curator_type");
        ms_roleToString.insert(CuratorsModel::ROLE_TITLE,"curator_title");
        ms_roleToString.insert(CuratorsModel::ROLE_TAG_LINE,"curator_tagline");
        ms_roleToString.insert(CuratorsModel::ROLE_BIO,"curator_bio");
        ms_roleToString.insert(CuratorsModel::ROLE_DATE_CREATED,"curator_date_created");
        ms_roleToString.insert(CuratorsModel::ROLE_IMAGE_FILE,"curator_image_file");
    }
}

#ifndef FREQUESTGENRES_H
#define FREQUESTGENRES_H

#include "FMA/FRequest.h"

class FRequestGenres : public FRequest
{
public:
    FRequestGenres( int curatorIndex = 0 );
};

#endif // FREQUESTGENRES_H

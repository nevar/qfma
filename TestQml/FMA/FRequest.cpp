#include "FRequest.h"

QHash<FRequest::RequestType,QString> FRequest::m_requestTypes = FRequest::initRequestTypes();
QHash<FRequest::ResponseType,QString> FRequest::m_responseTypes = FRequest::initResponseTypes();

uint qHash(const FRequest & fRequest)
{
    return 0;
}

FRequest::FRequest(int requestedIndex)
    : m_responseType( RESPONSE_TYPE_JSON )
    , m_limit(20)
    , m_isSortedAscending(true)
{
    setRequestIndex( requestedIndex );
}

FRequest& FRequest::internalCopy(const FRequest &other)
{
    m_requestType = other.m_requestType;
    m_responseType = other.m_responseType;
    m_limit = other.m_limit;
    m_page = other.m_page;
    m_requestedIndex = other.m_requestedIndex;
    m_sortId = other.m_sortId;
    m_isSortedAscending = other.m_isSortedAscending;

    return *this;
}

FRequest::FRequest(const FRequest &other)
{
    internalCopy(other);
}

FRequest &FRequest::operator=(const FRequest &other)
{
    return internalCopy(other);
}

bool FRequest::operator==(const FRequest &other) const
{
    return m_requestType == other.m_requestType &&
           m_responseType == other.m_responseType &&
           m_page == other.m_page &&
           m_limit == other.m_limit &&
           m_sortId == other.m_sortId &&
           m_isSortedAscending == other.m_isSortedAscending;
}

bool FRequest::operator!=(const FRequest &other) const
{
    return !(*this == other);
}

int FRequest::limit() const
{
    return m_limit;
}

void FRequest::setLimit(const int newLimit)
{
    m_limit = newLimit;
}

FRequest::ResponseType FRequest::responseType() const
{
    return m_responseType;
}

void FRequest::setResponseType(const FRequest::ResponseType responseType)
{
    m_responseType = responseType;
}

QString FRequest::getStringResponseType() const
{
    return m_responseTypes[m_responseType];
}

FRequest::RequestType FRequest::requestType() const
{
    return m_requestType;
}

void FRequest::setRequestType(const FRequest::RequestType requestType)
{
    m_requestType = requestType;
}

QString FRequest::getStringRequestType() const
{
    return m_requestTypes[m_requestType];
}

int FRequest::requestedIndex() const
{
    return m_requestedIndex;
}

void FRequest::setRequestIndex(const int newIndex)
{
    if ( newIndex < 0 )
    {
        m_requestedIndex = newIndex;
        m_page = 1;
    }
    else
    {
        m_requestedIndex = newIndex;
        m_page = ( m_requestedIndex / m_limit ) + 1;
    }
}

int FRequest::page() const
{
    return m_page;
}

void FRequest::setSordId(const QString &newSortId)
{
    m_sortId = newSortId;
}

QString FRequest::sortId() const
{
    return m_sortId;
}

void FRequest::setSortedAscending(bool state)
{
    m_isSortedAscending = state;
}

bool FRequest::isSortedAscending() const
{
    return m_isSortedAscending;
}

QHash<FRequest::RequestType, QString> FRequest::initRequestTypes()
{
    QHash<FRequest::RequestType, QString> hash;

    hash.insert( REQUEST_TYPE_CURATORS, "curators" );
    hash.insert( REQUEST_TYPE_GENRES, "genres" );

    return hash;
}

QHash<FRequest::ResponseType, QString> FRequest::initResponseTypes()
{
    QHash<FRequest::ResponseType, QString> hash;

    hash.insert(RESPONSE_TYPE_XML, "xml");
    hash.insert(RESPONSE_TYPE_JSON, "json");

    return hash;
}

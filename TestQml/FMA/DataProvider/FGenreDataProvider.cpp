#include "FMA/DataProvider/FGenreDataProvider.h"
#include "FMA/FRequest/FRequestGenres.h"

FGenreDataProvider::FGenreDataProvider()
    : QObject(NULL)
    , m_maxGenresCount(0)
{
    connect( &FSession::instance(), SIGNAL(genreLoaded(FGenre,int,int)), this, SLOT(onGenreLoaded(FGenre,int,int)) );
}

int FGenreDataProvider::maxGenresCount() const
{
    if ( m_genres.count() == 0 )
    {
        FRequestGenres f(-1);
        FSession::instance().sendFRequest(f);
    }
    return m_maxGenresCount;
}

FGenre FGenreDataProvider::getGenre(const int genreIndex) const
{
    if ( m_genres.contains(genreIndex) )
    {
        return m_genres[genreIndex];
    }
    else
    {
        FRequestGenres f(-1);
        FSession::instance().sendFRequest(f);
    }

    return FGenre();
}

void FGenreDataProvider::onGenreLoaded(FGenre genre, int index, int maxCurators)
{
    if ( m_maxGenresCount != maxCurators )
    {
        if ( m_maxGenresCount != 0 )
        {
            emit beginReset();
            m_genres.clear();
            m_maxGenresCount = 0;
            emit endReset();
        }

        if ( maxCurators > 0 )
        {
            emit beginInsertGenre( 0, maxCurators - 1 );
            m_maxGenresCount = maxCurators;
            emit endInsertGenre();
        }
    }

    m_genres[index] = genre;
    emit genreChanged(index);
}

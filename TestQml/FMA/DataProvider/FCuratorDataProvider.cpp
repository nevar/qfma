#include "FCuratorDataProvider.h"
#include "FMA/FRequest/FRequestCurators.h"
#include "FMA/FSession.h"
#include "FMA/Model/CuratorsModel.h"

FCuratorDataProvider::FCuratorDataProvider(CuratorsModel* pCuratorsModel)
    : QObject(NULL)
    , m_maxCuratorsCount(0)
    , m_pCuratorsModel(pCuratorsModel)
{
    FSession::instance().setApiKey("NKC2LLXO1TXCQMTZ");

    connect( &FSession::instance(), SIGNAL(curatorLoaded(FCurator,int,int)), this, SLOT(onCuratorLoaded(FCurator,int,int)) );
}

int FCuratorDataProvider::maxCuratorsCount() const
{
    if ( m_curators.count() == 0 )
    {
        FRequestCurators f( m_pCuratorsModel->sortingField() );
        f.setSortedAscending( m_pCuratorsModel->isSortedAscending() );
        FSession::instance().sendFRequest( f );
    }

    return m_maxCuratorsCount;
}

FCurator FCuratorDataProvider::getCurator(const int curatorIndex) const
{
    if ( m_curators.contains(curatorIndex) )
    {
        return m_curators[curatorIndex];
    }
    else
    {
        FRequestCurators f( curatorIndex, m_pCuratorsModel->sortingField() );
        f.setSortedAscending( m_pCuratorsModel->isSortedAscending() );
        FSession::instance().sendFRequest( f );
    }

    return FCurator();
}

void FCuratorDataProvider::onCuratorLoaded(FCurator curator, int index, int maxCurators)
{
    if ( m_maxCuratorsCount != maxCurators )
    {
        if ( m_maxCuratorsCount != 0 )
        {
            emit beginReset();
            m_curators.clear();
            m_maxCuratorsCount = 0;
            emit endReset();
        }

        if ( maxCurators > 0 )
        {
            emit beginInsertCurator( 0, maxCurators - 1 );
            m_maxCuratorsCount = maxCurators;
            emit endInsertCurator();
        }
    }

    m_curators[index] = curator;
    emit curatorChanged(index);
}

void FCuratorDataProvider::onSortingFieldChanged()
{
    emit beginReset();
    m_curators.clear();
    emit endReset();
}

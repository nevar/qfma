#ifndef FGENREDATAPROVIDER_H
#define FGENREDATAPROVIDER_H

#include "FMA/FGenre.h"
#include "FMA/FSession.h"
#include <QObject>
#include <QHash>

class FGenreDataProvider : public QObject
{
    Q_OBJECT
public:
    explicit FGenreDataProvider();

    int maxGenresCount() const;

    FGenre getGenre( const int genreIndex ) const;

signals:
    void genreChanged( int genreIndex );

    void beginInsertGenre( int genreStartIndex, int genreEndIndex );

    void endInsertGenre();

    void beginReset();
    void endReset();

public slots:
    void onGenreLoaded( FGenre genre, int index, int maxCurators );

private:
    QHash<int,FGenre> m_genres;

    int m_maxGenresCount;
};

#endif // FGENREDATAPROVIDER_H

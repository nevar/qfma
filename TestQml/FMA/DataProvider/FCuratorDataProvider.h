#ifndef FCURATORDATAPROVIDER_H
#define FCURATORDATAPROVIDER_H

#include "FMA/FCurator.h"
#include <QObject>
#include <QHash>

class CuratorsModel;

class FCuratorDataProvider : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief FCuratorDataProvider
     */
    explicit FCuratorDataProvider(CuratorsModel* pCuratorsModel);

    /**
     * @brief Returns maximal curators count
     * @return
     */
    int maxCuratorsCount() const;

    /**
     * @brief Returns copy of curator object from requested index
     * @param Curator index
     * @return Curator object
     */
    FCurator getCurator( const int curatorIndex ) const;

signals:
    /**
     * @brief Signal emited when existing curator changes its state
     * @param curatorIndex
     */
    void curatorChanged( int curatorIndex );

    /**
     * @brief Signal emit when new curator will be inserted
     * @param curatorIndex
     */
    void beginInsertCurator( int curatorStartIndex, int curatorEndIndex );

    /**
     * @brief Signal emit after inserting new curator
     */
    void endInsertCurator();

    void beginReset();
    void endReset();

public slots:
    /**
     * @brief onCuratorLoaded
     * @param curator
     * @param index
     */
    void onCuratorLoaded( FCurator curator, int index, int maxCurators );

    void onSortingFieldChanged();

private:

private:
    QHash<int,FCurator> m_curators; /// Curators map

    int m_maxCuratorsCount; /// Number of curators on the server
    CuratorsModel* m_pCuratorsModel;
};

#endif // FCURATORDATAPROVIDER_H

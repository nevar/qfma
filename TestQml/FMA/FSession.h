#ifndef FSESSION_H
#define FSESSION_H

#include "FMA/FRequest/FRequestCurators.h"
#include "FMA/FCurator.h"
#include "FMA/FGenre.h"
#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QMutexLocker>

class FSession : public QObject
{
    Q_OBJECT
public:
    Q_PROPERTY(QString apiKey READ apiKey WRITE setApiKey NOTIFY apiKeyChanged);


    static FSession& instance();
    
    QString apiKey() const;
    void setApiKey( const QString & key );

    /**
     * @brief Send FRequest for specific curator.
     * @param fRequest
     */
    void sendFRequest(const FRequest &fRequest );

private:
    explicit FSession(QObject *parent = 0);

signals:
    void apiKeyChanged( QString & apiKey );
    void curatorLoaded( FCurator curator, int index, int maxCurators);
    void genreLoaded( FGenre genre, int index, int maxGenres);

public slots:
    void onFinished( QNetworkReply * reply );
    
private:
    QNetworkAccessManager m_networkAccessManager;
    QString m_apiKey;

    QMutex m_mapLock;
    QHash<FRequest,int> m_sentRequests;
    QHash<QNetworkReply*,FRequest> m_requestsMap;
};

#endif // FSESSION_H
